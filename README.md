Plugin to launch stellar science TestRunner

Testing without a real build.

Steps to launch test web interface

mvn hpi:run

Navigate to https://localhost:8080/jenkins

Steps inside web interface
1. Create a job
2. Add a build step using the Test Runner Plugin. Configure as follows.
	
	Execute TestRunner
		TestRunner Binary Path: BuildArea/bin/TestRunner
		TestRunner Launch Dir: 
		Timeout:				180
		Log Level:				all
		Library Dir:			BuildArea/lib
		Test Results File:		test_reports/Thisisnotyourday.xml
		Test Runner Log File:	test_reports/TestRunnerLog.txt


		Test Selection
			Included Tests
				Test Name: Continuous::.*

3.  Run the build the first time. It will fail.
4.  In your source tree, navigate to
		work/jobs/<jobname>/workspace
5.  Run these commands in the above directory.
		mkdir -p BuildArea/bin
		mkdir old_reports
		mkdir test_reports
	
6.  At the root of the source tree, run this command
		copy extra/TestRunner work/jobs/<jobname>/workspace/BuildArea/bin
		copy extra/sample_report.xml work/jobs/<jobname>/workspace/BuildArea/bin

7.  Now rerun the build. It should succeed after finding the mock TestRunner
	and datafile.

