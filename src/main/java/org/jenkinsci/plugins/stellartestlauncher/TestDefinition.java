package org.jenkinsci.plugins.stellartestlauncher;

import org.kohsuke.stapler.DataBoundConstructor;

public class TestDefinition {
    private String testname;

    @DataBoundConstructor
    public TestDefinition(String testname) {
        setTestname(testname);
    }

    public String getTestname() {
        return testname;
    }

    public void setTestname(String testname) {
        this.testname = testname;
    }
}
