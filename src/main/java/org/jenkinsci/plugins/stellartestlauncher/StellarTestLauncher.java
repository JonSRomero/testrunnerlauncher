package org.jenkinsci.plugins.stellartestlauncher;

import hudson.Launcher;
import hudson.Extension;
import hudson.FilePath;
import hudson.EnvVars;
import hudson.util.FormValidation;
import hudson.util.ArgumentListBuilder;
import hudson.model.AbstractBuild;
import hudson.model.BuildListener;
import hudson.model.AbstractProject;
import hudson.tasks.Builder;
import hudson.tasks.BuildStepDescriptor;
import hudson.AbortException;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.QueryParameter;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Sample {@link Builder}.
 *
 * <p>
 * When the user configures the project and enables this builder,
 * {@link DescriptorImpl#newInstance(StaplerRequest)} is invoked
 * and a new {@link StellarTestLauncher} is created. The created
 * instance is persisted to the project configuration XML by using
 * XStream, so this allows you to use instance fields (like {@link #name})
 * to remember the configuration.
 *
 * <p>
 * When a build is performed, the {@link #perform(AbstractBuild, Launcher, BuildListener)}
 * method will be invoked.
 *
 * @author Kohsuke Kawaguchi
 */
public class StellarTestLauncher extends Builder {

    private final List<TestDefinition> testnames;

    private final List<TestDefinition> excludes;

    private final String libdir;

    private final String outfile;

    private final String logfile;

    private final String testlaunchdir;

    private final String testbinary;

    private final String additionalargs;

    private final String timeout;

    private final String log_level;


    // Fields in config.jelly must match the parameter names in the "DataBoundConstructor"
    @DataBoundConstructor
    public StellarTestLauncher(List<TestDefinition> testnames, List<TestDefinition> excludes, String libdir, String outfile, String logfile, String testlaunchdir, String testbinary, String additionalargs, String timeout, String log_level) {

        if (testnames != null) {
            this.testnames = new ArrayList<TestDefinition>(testnames);
        } else {
            this.testnames = new ArrayList<TestDefinition>();
        }

        if (excludes != null) {
            this.excludes = new ArrayList<TestDefinition>(excludes);
        } else {
            this.excludes = new ArrayList<TestDefinition>();
        }

        this.libdir = libdir;
        this.outfile = outfile;
        this.logfile = logfile;
        this.testlaunchdir = testlaunchdir;
        this.testbinary = testbinary;
	this.additionalargs = additionalargs;
        this.timeout = timeout;
        this.log_level = log_level;
    }

    /**
     * We'll use this from the <tt>config.jelly</tt>.
     */
    public String getTestlaunchdir() {
        return testlaunchdir;
    }

    public String getTestbinary() {
        return testbinary;
    }

    public List<TestDefinition> getTestnames() {
        return testnames;
    }

    public List<TestDefinition> getExcludes() {
        return excludes;
    }

    public String getLibdir() {
        return libdir;
    }

    public String getOutfile() {
        return outfile;
    }

    public String getLogfile() {
        return logfile;
    }

    public String getAdditionalargs() {
	return additionalargs;
    }

    public String getTimeout() {
        return timeout;
    }

    public String getLog_level() {
        return log_level;
    }

    

    @Override
    public boolean perform(AbstractBuild build, Launcher launcher, BuildListener listener) throws IOException, InterruptedException, AbortException {

        listener.getLogger().println("We think our workspace root is , " + build.getWorkspace() + "!");

        final EnvVars envs = build.getEnvironment(listener);

        ArgumentListBuilder args = new ArgumentListBuilder();

        //Construct complete path to tester binary
        FilePath testerbinarypath = new FilePath(build.getWorkspace(), testbinary);

        //Ensure tester binary exists at specified path
        if (!testerbinarypath.exists()) {
            throw new AbortException("Tester binary does not exist at: " + testerbinarypath);
        }

        FilePath pwd = new FilePath(build.getWorkspace(), testlaunchdir);

        FilePath libdirpath = new FilePath(build.getWorkspace(), libdir);

        //Ensure library directory exists
        if (!libdirpath.exists()) {
            throw new AbortException("Library directory does not exist at: " + libdirpath);
        }

        FilePath outfilepath = new FilePath(build.getWorkspace(), outfile);
        FilePath logfilepath = new FilePath(build.getWorkspace(), logfile);

        //Ensure output file directory exists
        if (!outfilepath.getParent().exists()) {
            try {
                outfilepath.getParent().mkdirs();
            } catch (final Exception e) {
                throw new AbortException("Output file directory does not exist at: " + outfilepath.getParent() + ". Further, an attempt to automatically create this directory failed.");

            }
        }

        args.add(testerbinarypath);

        args.add("--libdir=" + libdirpath);

        args.add("--logger-file=" + logfilepath);
		
		args.add("--test-results-file=" + outfilepath);


        args.add("--timeout=" + timeout);

        args.add("--log_level=" + log_level);

        for (TestDefinition t : testnames) {
            args.add("--testname=" + t.getTestname());
        }

        for (TestDefinition e : excludes) {
            args.add("--exclude-testname=" + e.getTestname());
        }

	args.addTokenized(additionalargs);

  
            
			//.cmds(args + " " + additionalargs)
            
		launcher.launch()
			.cmds(args)
			.envs(envs)
			.pwd(pwd)
			.stdout(listener.getLogger())
			.stderr(listener.getLogger())
			.join();
	
  

        return true;
    }

    // Overridden for better type safety.
    // If your plugin doesn't really define any property on Descriptor,
    // you don't have to do this.
    @Override
    public DescriptorImpl getDescriptor() {
        return (DescriptorImpl) super.getDescriptor();
    }

    /**
     * Descriptor for {@link StellarTestLauncher}. Used as a singleton.
     * The class is marked as public so that it can be accessed from views.
     *
     * <p>
     * See <tt>src/main/resources/hudson/plugins/hello_world/StellarTestLauncher/*.jelly</tt>
     * for the actual HTML fragment for the configuration screen.
     */
    @Extension // This indicates to Jenkins that this is an implementation of an extension point.
    public static final class DescriptorImpl extends BuildStepDescriptor<Builder> {
        /**
         * To persist global configuration information,
         * simply store it in a field and call save().
         *
         * <p>
         * If you don't want fields to be persisted, use <tt>transient</tt>.
         */

        /**
         * In order to load the persisted global configuration, you have to
         * call load() in the constructor.
         */
        public DescriptorImpl() {
            load();
        }

        /**
         * Performs on the fly validation of the form field 'testbinary'
         */
        public FormValidation doCheckTestbinary(@QueryParameter String value)
            throws IOException, ServletException {
            if (!(value.length() > 0)) {
                return FormValidation.error("Test binary cannot be blank.");
            }
            return FormValidation.ok();
        }

        /**
         * Performs on the fly validation of the form field 'timeout'
         */
        public FormValidation doCheckTimeout(@QueryParameter String value)
            throws IOException, ServletException {
            if (!value.matches("^[1-9][0-9]*$")) {
                return FormValidation.error("Timeout is required ^[1-9][0-9]*$");
            }
            return FormValidation.ok();
        }

        public FormValidation doCheckTestname(@QueryParameter String value)
            throws IOException, ServletException {
            try {
                Pattern.compile(value);
            } catch (final PatternSyntaxException exception) {
                return FormValidation.error("Invalid testname. Must be a valid regex expression.");
            }
            return FormValidation.ok();
        }

        public boolean isApplicable(Class<? extends AbstractProject> aClass) {
            // Indicates that this builder can be used with all kinds of project types
            return true;
        }

        /**
         * This human readable name is used in the configuration screen.
         */
        public String getDisplayName() {
            return "Execute TestRunner";
        }

        @Override
        public boolean configure(StaplerRequest req, JSONObject formData) throws FormException {
            save();
            return super.configure(req, formData);
        }

    }
}
